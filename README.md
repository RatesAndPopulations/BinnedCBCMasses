Binned CBC masses using a Gaussian Process regularisation.

This package contains a Python executable and a Stan program that fit
a binned model to 2D data using a Gaussian process to regularise the
fit.  The method is similar to
[Foreman-Mackey, Hogg & Morton (2014)](http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:1406.3020)
with a few minor, technical differences.

The `population` executable's arguments are fully described by

```bash
population --help
```

but here we give an overview of the operation of the code.  The code
fits a piecewise-constant function
$$
\newcommand{\dd}{\mathrm{d}}
\frac{\dd N}{\dd V_c \dd t_R \dd \log m_1 \dd \log m_2}
$$
which is the differential rate per comoving time-volume per (natural)
log of each mass to samples in the m1--m2 plane.  The code requires
the following files to be available before a run:

1. A file (default name `mbins.dat`) giving the boundaries of the mass
   bins.  This file has a 1D array in text format, e.g. as produced by
   `np.savetxt`.  The code will enforce that `m1 >= m2`, so only the
   lower half mass plane will be fit for.

1. A shape file (default name `mu.dat`) which is a 2D matrix in text
   format, e.g. as output by `np.savetxt`, giving the shape of the
   mean function for the GP prior in the binned space.

1. A file giving the sensitive time-volume for the observations in
   each mass bin (default name `VTs.dat`).  This file has a 2D matrix
   in text format, e.g. as output by `np.savetxt`.  Each entry gives
   the sensitivity of the search in the corresponding bin, so that
   $$
   \frac{\dd N}{\dd V_c \dd t_R \dd \log m_1 \dd \log m_2}
   \left\langle VT \right\rangle \Delta \log m_1 \Delta \log m_2 = \lambda,
   $$
   where $\lambda$ is the expected number of detections in the bin.

1. A directory containing one file per detected system (default name
   `posteriors`).  Each file is a text 2D matrix with a first line
   giving column names, as could be read from `np.genfromtxt(...,
   names=True)`.  There must be columns named `m1_source` and
   `m2_source`, which give posterior samples in $m_1$--$m_2$ space
   drawn from a *flat prior* in $m_1$ and $m_2$.

Given these components, running the `posterior` executable will invoke
a `PyStan` process that will sample from the model and produce a file
(by default named `chains.pkl.bz2`) that contains the posterior chains
over the GP hyperparameters and the bin heights (actually the log of
the bin heights), plus various auxiliary variables defined to make
sampling the model easier.  You can load this file and extract the
posterior samples via

```python
import bz2
import pickle
import pystan

with bz2.BZ2File('chains.pkl.bz2', 'r') as inp:
	chain = pickle.load(inp)

mean_rates = np.mean(np.exp(chain['logn']), axis=0) # mean_rates.shape == (nbins,nbins)
```
