functions {
  matrix sq_exp_cov(vector[] xs, matrix g, real sigma, real wn_frac, int n, int ndim) {
    matrix[n,n] cm;
    real s2;

    s2 = sigma*sigma;

    for (i in 1:n) {
      for (j in i:n) {
	vector[ndim] dx;
	real r2;

       	dx = xs[i] - xs[j];

	r2 = quad_form(g, dx);

	cm[i,j] = s2*exp(-0.5*r2);
	cm[j,i] = cm[i,j];
      }
    }

    for (i in 1:n) {
      cm[i,i] = cm[i,i]*(1 + wn_frac);
    }

    return cm;
  }

  real[] corr_sq_exp(real[] ys, vector[] xs, real mu, real sigma, matrix g, real wn_frac, int n) {
    matrix[n,n] cm;
    matrix[n,n] ccm;
    vector[n] vys;
    vector[n] vzs;
    int ndim;

    ndim = dims(xs[1])[1];

    vys = to_vector(ys);

    cm = sq_exp_cov(xs, g, sigma, wn_frac, n, ndim);
    ccm = cholesky_decompose(cm);

    vzs = ccm*vys;
    vzs = vzs + mu;

    return to_array_1d(vzs);
  }
}

data {
  int nbins;
  int nevents;

  vector[nbins+1] bounds;

  matrix[nbins, nbins] weights[nevents];

  matrix[nbins, nbins] mu_log_alphas;
  matrix[nbins, nbins] sigma_log_alphas;

  real wn_frac;

  matrix[nbins, nbins] mu0;

  int prior_flag;
}

transformed data {
  matrix[nbins, nbins] deltas;
  vector[nbins] bin_cent;
  matrix[nbins, nbins] logwts[nevents];

  real dm_min;
  real dM;

  real mu_scale;
  real sigma_scale;

  int nhist;

  nhist = (nbins+1)*nbins/2;

  for (i in 1:nbins) {
    for (j in 1:nbins) {
      if (j > i) {
	       deltas[i,j] = 0.0; /* Only lower triangular, m2 <= m1 */
      } else if (i == j) {
	       deltas[i,j] = 0.5*(bounds[i+1]-bounds[i])*(bounds[j+1]-bounds[j]); /* Diagonal has half-bins*/
      } else {
	       deltas[i,j] = (bounds[i+1]-bounds[i])*(bounds[j+1]-bounds[j]);
      }
    }
  }

  for (i in 1:nbins) {
    bin_cent[i] = 0.5*(bounds[i+1]+bounds[i]);
  }

  for (i in 1:nevents) {
    logwts[i] = log(weights[i]);
  }

  dm_min = bounds[2]-bounds[1];
  for (i in 2:nbins) {
    real dm;
    dm = bounds[i+1]-bounds[i];
    if (dm < dm_min) {
      dm_min = dm;
    }
  }

  dM = bounds[nbins+1]-bounds[1];

  {
    real scale_min;
    real scale_max;

    scale_min = dm_min;
    scale_max = dM;

    mu_scale = 0.5*(log(scale_min) + log(scale_max));
    sigma_scale = (log(scale_max) - log(scale_min))/4.0;
  }
}

parameters {
  vector[nhist] rawlogn;
  matrix[nbins, nbins] rawloga;

  real mu;
  real<lower=0> sigma;
  real<lower=0> lambda;
}

transformed parameters {
  matrix[nbins,nbins] logn;
  matrix[nbins,nbins] log_alphas = mu_log_alphas + sigma_log_alphas .* rawloga;

  {
    int ivec;
    real logn1d[nhist];
    vector[2] binvs[nhist];
    matrix[nhist, nhist] covm_gp;
    matrix[nhist, nhist] covm;
    matrix[nhist, nhist] chol_covm;

    ivec = 1;
    for (i in 1:nbins) {
      for (j in i:nbins) {
        binvs[ivec][1] = bin_cent[i];
	      binvs[ivec][2] = bin_cent[j];
	      ivec = ivec + 1;
      }
    }

    covm_gp = cov_exp_quad(binvs, sigma, lambda);

    /* Here we add the diagonal jitter term. */
    covm = covm_gp + diag_matrix(wn_frac/(1-wn_frac)*diagonal(covm_gp));

    chol_covm = cholesky_decompose(covm);

    logn1d = to_array_1d(chol_covm * rawlogn + mu);

    ivec = 1;
    for (i in 1:nbins) {
      for (j in i:nbins) {
	       logn[i,j] = logn1d[ivec] + mu0[i,j];
         logn[j,i] = logn[i,j];
         ivec = ivec + 1;
      }
    }
  }
}

model {
  real excounts;

  /* Priors */
  to_array_1d(rawlogn) ~ normal(0,1); /* Will be correlated into the transformed parameters. */
  to_array_1d(rawloga) ~ normal(0,1); /* Will be scaled and shifted into log-normal alpha distribution. */

  mu ~ normal(0.0, 10.0); /* Wide prior on normalisation. */

  sigma ~ normal(0, 1);
  lambda ~ lognormal(mu_scale, sigma_scale);

  if (prior_flag == 1) {
    /* Do Nothing---we've already go the prior. */
  } else {
    excounts = sum(deltas .* exp(logn + log_alphas));

    for (i in 1:nevents) {
	     target += log_sum_exp(logwts[i] + logn);
    }
    target += -excounts;
  }
}
